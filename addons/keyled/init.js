d.addEventListener("keyDown", (e) => {
  let event = e.detail
  let keyCode = event.keyCode
  let char = event.key
  // roughly alphadecimal + ctrl keys
  if (keyCode >= 48 || (keyCode >= 9 && keyCode <= 19)) { 
    let keyLed = d.createElement('div')
    keyLed.classList.add('keyled')
    keyLed.dataset.keycode = keyCode
    keyLed.innerHTML = char
    console.log(char);
    keyLed.setAttribute('style', `--kc:${keyCode};`)
    messageBox.appendChild(keyLed)
    // in case keyup is not triggered (it often happens)
    setTimeout(() => {hideKey(keyCode)}, 1000);
  }
});

function hideKey(keyCode){
  let keyLeds = d.querySelectorAll('.keyled')
  keyLeds.forEach(keyLed => {
    if (keyLed.dataset.keycode == keyCode) {
      keyLed.remove()
    }
  });
}
