const width = 320;
let height = 0;
let streaming = false;
let videoElement = null;
let isCam = false

function cam() {
  if (isCam) {
    stopCam()
    return
  }
  videoElement = document.createElement("video");
  videoElement.style = "position:fixed; bottom:20%; right:0; opacity:.5; border-radius:10px;"
  document.body.appendChild(videoElement)
  if (!navigator.mediaDevices) {
    alert('Cam addon: use secured domain or localhost')
    return
  }
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: false })
    .then((stream) => {
      dragElement(videoElement);
      videoElement.srcObject = stream;
      videoElement.play();
    })
    .catch((err) => {
      console.error(`An error occurred: ${err}`);
    });

  videoElement.addEventListener(
    "canplay",
    (ev) => {
      if (!streaming) {
        height = videoElement.videoHeight / (videoElement.videoWidth / width);
        // Firefox currently has a bug where the height can't be read from
        // the video, so we will make assumptions if this happens.
        if (isNaN(height)) {
          height = width / (4 / 3);
        }
        videoElement.setAttribute("width", width);
        videoElement.setAttribute("height", height);
        streaming = true;
      }
    },
    false
  )
  isCam = true
}

// startCam()
function stopCam(){
  videoElement.remove()
  streaming = false
  videoElement = null
}

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}