# été Addons

- Should be located in `addons/name`, with name in camelCase
- Should have a `init.js` file with their own function
- Should create their own CSS stylesheet link in the page if needed
- It’s better if the main function to call is the name as the addon name, and this function is also a toggle to activate / deactivate the addon