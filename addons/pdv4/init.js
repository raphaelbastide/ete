var cssLink = document.createElement('link');
cssLink.rel = 'stylesheet';
cssLink.type = 'text/css';
cssLink.href = 'addons/pdv4/pdv4.css';
d.head.appendChild(cssLink);
b.dataset.theme = "bright"
let pdv4Started = false
function pdv4(){
  if (pdv4Started) {
    pdv4Started = false
  }else{
    d.addEventListener("percsynth", (e) => {
      generateBorders()
    })
    d.addEventListener("playpattern", (e) => {
      generateBorders()
    })
    pdv4Started = true
  }
}
pdv4()

let cols = ["Aquamarine", "Pink", "blue", "LightGreen","LightSalmon", "black", "CornflowerBlue","DarkGreen","LightBlue","DarkOrange","Khaki"]
let types = ["solid", "groove", "inset", "outset"]

function generateBorders() {
  el = b.querySelector('.editor')
  let borderCol = cols[Math.round(Math.random() * cols.length)]
  let borderTypes = types[Math.round(Math.random() * types.length)]
  el.style.border = `4vw ${borderTypes} ${borderCol}`
}
