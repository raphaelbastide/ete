let isInit = false
let dlCount = 0
// max hits per before next logo (10)
let hits = 10
let currentSVG
let logoList
let logoBox

theme('bright')

async function getLogoList(path){
  return new Promise((resolve) => {
    fetch(path, {cache: "no-store"})
      .then(response => response.text())
      .then((sheet) => {
        resolve(sheet)
      })
  })
}

async function init(){
  // get logo list
  logoList = await getLogoList('addons/destroyLogos/logos.json')
  logoList = JSON.parse(logoList)
  // Create logo box
  logoBox = d.createElement('div')
  logoBox.classList.add('logo-box')
  const styleTag = d.createElement('style');
  styleTag.textContent = `.logo-box{position:fixed; top:0; left:calc((100vw - 70vw) / 2); pointer-events:none; z-index:-1;}
  .logo-box svg{width:70vw; height:100vh; object-fit: contain; filter:saturate(1); opacity:.7;}`
  d.head.appendChild(styleTag);
  b.appendChild(logoBox)
  // attach keyDown event
  d.addEventListener("keyDown", nextLogo,false)
  isInit = true
}

function destroyLogos(){
  if (!isInit) {
    init()
  }else{
    logoBox.innerHTML = ""
    d.removeEventListener("keyDown", nextLogo, false);
  }
}

function nextLogo(){
  if (dlCount % hits == 0) {
    injectSVG(dlCount / hits)
  }
  let paths = currentSVG.querySelectorAll('path')
  paths.forEach(path => {
    destroyPath(path, dlCount)
  });
  dlCount++
}

function injectSVG(){
  // get undestroyed SVG only
  let undestroyedSVG = logoList.logos.filter(function(itm){
    return itm.destroyed == false
  })
  // pick a random logo from all undestroyed logos
  let rLogo = undestroyedSVG[Math.floor(Math.random() * undestroyedSVG.length)]
  if (rLogo !== undefined) {
    // set the logo as destroyed
    console.log(rLogo.name);
    rLogo['destroyed'] = true
    // inject logo
    logoBox.innerHTML = rLogo.svg
    // pass currentSVG for path destruction
    currentSVG = logoBox.querySelector('svg')
  }else{
    // rLogo is undefined:
    // set all logos as undestroyed to started again
    logoList.logos.forEach(logo => {
      logo['destroyed'] = false
    })
    injectSVG()
  }
}

function destroyPath(path, dlCount) {
  dlCount = dlCount % 2
  let pathData = path.getAttribute('d')
  let commands = pathData.split(/(?=[A-Za-z])/);
  if(commands[dlCount]){
    let [command, ...args] = commands[dlCount].split(/[\s,]+/);
    if (dlCount > commands.length) {
      return
    }
    commands[dlCount] = `${command}${args.join(' ')}`;
  }
  let newPathString = commands.join('');
  path.setAttribute('d', newPathString);
}
