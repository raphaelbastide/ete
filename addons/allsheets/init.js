let isAllSheets = true
let link = document.createElement('link')
link.rel = 'stylesheet'
link.type = 'text/css'
link.href = 'addons/allsheets/allsheets.css'
d.head.appendChild(link)
let asFrame

function allSheets(){
  if (!isAllSheets) {
    asFrame.delete()
    isAllSheets = false
  }else{
    asFrame = document.createElement('iframe')
    asFrame.classList.add('as-frame')
    asFrame.src = 'addons/allsheets/list.php'
    document.body.appendChild(asFrame)
    isAllSheets = true
  }
}
allSheets()

window.addEventListener(
  "message",
  (event) => {
    console.log(event.data);
    load("sheets/"+event.data)
  },
  false,
);