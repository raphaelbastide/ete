let texts =
[
  "A machine is really yours  when you break it",
  "Times are dark, sounds are bright",
  "Every single line of code is political",
  "Grooves are subjective",
  "A good tool resists",
  "Revolution is not waiting",
  "Is the journey still important when chaos is the destination",
  "Make your own tools, use your own knowledge",
  "This code will be obsolete in less than 10 years",
  "A bouquet of errors",
  "A web browser is a tool of power",
  "Music is a pretext to be together"
]

var link = document.createElement('link');
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = 'addons/quotes/stylesheet.css';
d.head.appendChild(link);
let textBox = d.createElement("div")
textBox.classList.add("text-box")
textBox.style = `position: absolute;
right: 0px;
top: 0px;
height: 100vh;
font-size: 10vw;
font-family:scriptina;
color:navy;
width: 70%;
line-height: 1.2em;
padding: 0em 1em 0 0;
z-index:3;
pointer-events: none;
text-align: right;`
b.appendChild(textBox)

let count = 0
setInterval(() => {
  if (count == texts.length) {
    count = 0
  }
  textBox.innerHTML = texts[count]
  count++
}, 1000 * 20);