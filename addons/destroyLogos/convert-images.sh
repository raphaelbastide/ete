#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# First remove everything existing in current dir
rm "$DIR"/optimised-logos/*.svg

# then copy the images in ./min
cp original-logos/*/*.svg ./optimised-logos

# Optimise SVGs using svgo
svgo -p 4 --config=svgo-config.js optimised-logos/*.svg -o ./optimised-logos

# for f in "$DIR"/optimised-logos/*.svg;
#   do
#   # Convert in grayscale with inkscape
#   inkscape --actions EditSelectAll --actions org.inkscape.color.desaturate.noprefs -o $f $f 
#   done


notify-send -t 8000 "End of image processing";
