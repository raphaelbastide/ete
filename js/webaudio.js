let baseNote = new Note()
let hasReverb

// get fm programs
let request_programs = new XMLHttpRequest();
// can be called in /synth-editor/ context
let basePath = window.location.pathname.includes('/synth-editor/') ? '../js/allsynths.json' : './js/allsynths.json';
// let basePath = window.location.pathname.includes('/synth-editor/') ? '../js/custom-synths.json' : '/js/custom-synths.json';
request_programs.open("GET", basePath, false);
request_programs.send(null);

let programs = JSON.parse(request_programs.responseText)["programs"];
let ctx // Audio Context

window.addEventListener("mousedown", checkAudioCtx);
window.addEventListener("keydown", checkAudioCtx);
function checkAudioCtx(){
  if(ctx == undefined || ctx.state == "closed" || ctx.state == "suspended"){
    ctx = new(window.AudioContext || window.webkitAudioContext)();
    console.log("context created");
  }
} 

function playNote(note, synth, panVal=0, pattern=0) {  
  hasReverb = false
  if (ctx == undefined) return
  // do not play if gain or note = 0:
  if(synth.gain != 0 && note != "0" && note != 0){
    // Main osc
    let oscillators
    let release = 0

    // Parse floats so minor typo don’t fatal error
    synth.dur = parseFloat(synth.dur)
    synth.gain = parseFloat(synth.gain)

    // handles multisynths types (synth type names separated by underscores)
    if (synth.type.includes("_")) {
      multiSynth = synth.type.split("_")
      multiSynth.forEach(synthType => {
        synth.type = synthType
        playNote(note, synth, panVal)
      })
      return
    }
    // same for notes. Things need to be dirty sometimes for life to be fun
    if (note.toString().includes("_")) {
      multiSynth = note.toString().split("_")
      // no multisynth for noise, as it clogs the gain
      if (synth.type == "noise") {
        playNote(multiSynth[0], synth, panVal)
      }else{
        multiSynth.forEach(synthNote => {
          note = synthNote
          playNote(note, synth, panVal)
        })
      }
      return
    }

    // Main gain 
    const mainGain = ctx.createGain()
    mainGain.gain.value = synth.gain 
    // changing envelope string to array, doing that in solve() creates a mess
    env = synth.env.split(" ").join("").split(',')
    // map() relates gain vals to mainGain
    mainGain.gain.setValueCurveAtTime(new Float32Array(env).map(function(x){return x * synth.gain}), ctx.currentTime, synth.dur); 
    
    // Panner
    let panNode = ctx.createStereoPanner() 
    panNode.pan.value = panVal; 

    note = convertNote(note)
    if ((!hasMidi && pattern.state == "m-out") || (!hasMidi && pattern.state == "m-out")) {
      message("MIDI unsupported","error")
    }
    if (pattern.state == "m-out" && hasMidi) {
      midinote = note
    }
    // if basic synths
    if(synth.type=="sine" || synth.type=="square" || synth.type=="sawtooth" || synth.type=="triangle" || synth.type=="noise" || synth.type=="onoise"){
      oscillators = basicOSC(synth.type, synth.gain, synth.dur,note)
      oscillators[0].connect(panNode)
    }else{
      let timbrePatch
      if(synth.type.includes("[{")){ // raw FM patch with fm(…)
        // adds commas and quotemarks back
        timbrePatch = synth.type.replace(/"/g,"").replace(/œ/g,',').replace(/'/g, '"')
        // string to object
        timbrePatch = JSON.parse(timbrePatch)
      }else{
        // FM synth by name
        timbrePatch = timbrePatchFromName(synth.type)
      }
      let timbre = createTimbre(timbrePatch, note, synth.dur);
      oscillators = timbre[0] // getting back osc list
      timbre[1].forEach(g=>{
        g.connect(panNode)
      })
      release = timbre[2];
    }

    // effects
    oscillators.forEach(osc => {
      getEffects(note, synth, panVal, osc);
      if (hasReverb) {
        let conv_rev = tinyReverb(hasReverb[0])
        let conv = conv_rev[0]
        let rev = conv_rev[1]
        mainGain.connect(conv)
        conv.connect(rev)
        rev.connect(ctx.destination)
        }    
    });

    // panner depends on the main gain
    panNode.connect(mainGain) 
    // mainGain comes from panNode
    mainGain.connect(ctx.destination)
    
    // looping through oscillators to start
    for(i = 0; i < oscillators.length; i++){
      oscillators[i].start(ctx.currentTime)
      oscillators[i].stop(ctx.currentTime + synth.dur + release)
    }
  }  
  if (pattern.state == "m-out") {
    if (hasMidi) {      
      let output = WebMidi.outputs[0]
      if (midinote > 127) { midinote = 127} // arbitrary, avoid non-midi notes.
      output.playNote(midinote, 1, {duration: synth.dur, velocity: synth.gain})
    }
  }
}

// Convert Notes
function convertNote(note){
  if(isNaN(note)){
    note = note.toUpperCase()
    if (baseNote.note2freq[note]) {
      // note to freq
      return baseNote.note2freq[note]
    }else{
      // returns 0 if note does not exist (ex. E#3)
      return 0
    }
  }else{
    // return a note only if number
    return note
  }
}

function algoReverb(seconds, decay, reverse){
  let rate = ctx.sampleRate
  let length = rate * seconds
  // createBuffer is deprecated, change it to decodeAudioData()
  let impulse = ctx.createBuffer(1, length, rate)
  let impulseC = impulse.getChannelData(0)
  let n, i
  for (i = 0; i < length; i++) {
    n = (reverse) ? length-i : i
    impulseC[i] = (Math.random()*2 - 1) * Math.pow( 1 - n/length, decay)
  }
  return impulse
}

function tinyReverb(level){ //from tinysynth by g200kg
  let blen=ctx.sampleRate*.5|0
  let convBuf=ctx.createBuffer(2,blen,ctx.sampleRate)
  var d1=convBuf.getChannelData(0)
  var d2=convBuf.getChannelData(1)
  for(let i=0;i<blen;++i){
    if(i/blen<Math.random()){
      d1[i]=Math.exp(-3*i/blen)*(Math.random()-.5)*.5
      d2[i]=Math.exp(-3*i/blen)*(Math.random()-.5)*.5
    }
  }
  conv=ctx.createConvolver()
  conv.buffer=convBuf
  let rev =ctx.createGain()
  level = level*8
  rev.gain.value=level
  return([conv, rev])
}

function getEffects(note, synth, panVal, osc){
  let effectName
  if ((synth.fx !== "0"||synth.fr!=="null") && (typeof synth.fx === 'string' || synth.fx instanceof String)) {
    // string to array
    synth.fx = synth.fx.split("»")
    effectName = synth.fx[0]
  }
  if (effectName =="expo") {
    // exponential
    let value = synth.fx[1]
    let time = synth.fx[2]
    // time should be relative to beat duration, so 0<t>1
    time = time * synth.dur
    if(osc['frequency'] == undefined){ message('Wrong synth type', 'error'); return}
    return osc['frequency'].exponentialRampToValueAtTime(value, ctx.currentTime + time);
  }else if(effectName =="rev"){
    // reverb
    let rev_lev = synth.fx[1];
    return hasReverb = [rev_lev];
  }else if(effectName =="del"){
    // delay
    let delayTime = synth.fx[1]
    let feedback = Math.floor(synth.fx[2])
    let accel = synth.fx[3]
    for (let i = 1; i <= feedback; i++) {
      setTimeout(() => {
        synth.gain = synth.gain / 2
        playNote(note, synth, panVal)
      }, delayTime / feedback * i / accel * 1000);
    }
  } else {return}
}

// White noise
function whiteNoise(ctx, ampl, dur){
  // 10 is arbirtrary time in sec for the buffer to last, long enough to cover most cases, will be overwriten by beat duration anyway.
  const whiteBuffer = ctx.createBuffer(2, ctx.sampleRate*dur, ctx.sampleRate)
  for (let ch=0; ch<whiteBuffer.numberOfChannels; ch++) {
    let samples = whiteBuffer.getChannelData(ch)
    for (let s=0; s<whiteBuffer.length; s++) samples[s] = Math.random()* ampl * .5- ampl
  }
  return new AudioBufferSourceNode(ctx, {buffer:whiteBuffer})
}

// onoise
function noiseOscillator(ctx, note, dur, res, noise, flat, dist){
  // thx adl!
  let pattern = []
  let i = 0
  while (i < ctx.sampleRate/note) {
   let rdm_val = Math.random() - Math.random()
    let val = (Math.sin(2*Math.PI*note * (i/ctx.sampleRate)) + rdm_val * noise) * dist
    val = Math.floor(val * res)/res
    for (var r = 0; r < flat; r++) {
      pattern.push(val > 1 ? 1 : val < -1 ? -1 : val)
      i++;
    }
  }
  const buffer = ctx.createBuffer(1, ctx.sampleRate*dur, ctx.sampleRate)
  for (let ch=0; ch<buffer.numberOfChannels; ch++) {
    let samples = buffer.getChannelData(ch)
    for (let s=0; s<buffer.length; s++) {
      samples[s] = pattern[s > pattern.length-1 ? s - Math.floor(s/pattern.length) * pattern.length : s ]
    }
  }
  return new AudioBufferSourceNode(ctx, {buffer:buffer})
}

function clamp(val, min, max) {
  return val > max ? max : val < min ? min : val;
}

function timbrePatchFromName(program_name){
  return programs[program_name]["patch"]; 
}

function createTimbre(synth_patch, note, duration){
  const oscillators = []
  const gains = []
  let carrier_release
  for(let i = 0; i < synth_patch.length; i++) { // looping operators
    let op = synth_patch[i]
      let osc = ctx.createOscillator()
      let gain = ctx.createGain()
      if(op["w"] == "w9999"){
        osc.setPeriodicWave(createWave(op["w"]));
      }else if(op["w"]=="n1"){
        osc = noise1()
        osc.loop = true
      }else if(op["w"]=="n0"){
        osc = noise0()
        osc.loop = true
      }else{
        osc.type = op["w"]; // assigning wave type
      }
      if(i == 0){// if on carrier-wave
        let t = op["t"]
        let f
        (t == undefined) ? t = 1.0 : t=t.toFixed(2);
        (op["f"]) ? f = op["f"] : f = 0;
        if(!osc.frequency){              
          osc.playbackRate.value = (note * t + f)/440
        }else{
          osc.frequency.value = note * t + f
        }
        (op["r"] == undefined) ? carrier_release = 0. : carrier_release = op["r"] // defines carrier release
        gain = adsr(ctx, gain, duration, op["v"], op["a"], op["d"],op["h"],op["s"],op["r"], true) // adsr
        osc.connect(gain)
        gains[i] = gain
      }else{
        let t = op["t"]
        let f
        let ogfreq
        (op["f"]) ? f = op["f"] : f = 0;
        (t == undefined) ? t = 1.0 : t=t.toFixed(2);
        // if noise, plays playback rate
        (oscillators[0].frequency) ? ogfreq = oscillators[0].frequency.value : ogfreq = oscillators[0].playbackRate.value;
        if(op["g"]==0 || op["g"]==undefined){
          ogfreq = note
          gain = adsr(ctx, gain, duration, (op["v"]), op["a"], op["d"],op["h"],op["s"],op["r"]) // adsr
        }else{
        gain = adsr(ctx, gain, duration, (op["v"]*ogfreq), op["a"], op["d"],op["h"],op["s"],op["r"]) // adsr
        }
        if(!osc.frequency){              
          osc.playbackRate.value = (ogfreq * t + f)/440
        }else{
          // modulateur.frequence = t * notecarrier
          osc.frequency.value = ogfreq * t + f;
        }
        if(op["g"] != undefined && op["g"]!=0){
          if(!oscillators[op["g"]-1].frequency){
            gain.connect(oscillators[op["g"]-1].playbackRate)
          }else{
            gain.connect(oscillators[op["g"]-1].frequency)
          }
        }else{
          gains[i] = gain
        }
        osc.connect(gain)
      }
      if(op["p"]!=undefined && op["w"]!="n0"){
        pitchBend(ctx, osc, duration, parseFloat(op["p"]), op["q"])
      }
      oscillators[i] = osc
  }
  // getting back osc list, gain and release
  return [oscillators, gains, carrier_release] 
}

// adsr(ctx, gain, duration, volume, attack, decay, hold, sustain, release, carrier?)
function adsr(ctx,g, duration, v, a, d, h, s, r, carrier = false){ 
  (a == undefined) ? a = 0. : a=a;
  (d == undefined) ? d = 0. : d=d;
  (h == undefined) ? h = 0. : h=h;
  (s == undefined) ? s = 0. : s=s;
  (r == undefined) ? r = 0. : r=r;
  //sustain de la carrier au moins 0.5 dans le cas où release
  (s<=0.)&&(carrier == true) ? (s=0.5) : (s=s)
  // calculate sustain
  s = s*v;
  g.gain.setValueAtTime(0, ctx.currentTime) // attack -> 0 to v
  g.gain.linearRampToValueAtTime(v, ctx.currentTime + a) // attack -> 0 to v
  g.gain.setValueAtTime(v, ctx.currentTime + a) // attack -> 0 to v
  g.gain.linearRampToValueAtTime(v, ctx.currentTime + a + h) 
  g.gain.linearRampToValueAtTime(s, ctx.currentTime + a + h + d) // decay -> v to s 
  g.gain.setValueAtTime(s, ctx.currentTime + duration) // attack -> 0 to v
  g.gain.linearRampToValueAtTime(0.01, ctx.currentTime + duration+r) // decay -> v to s 
  return g
}

function basicOSC(type, gain, dur, note){
  if(type == 'noise'){
    gain = gain + .2
    osc = whiteNoise(ctx, gain, dur)
  }else if(type == 'onoise'){
    osc = noiseOscillator(ctx, note, dur, 1, 1, 1, 1)
  }else if(type == 'sil'){
    return
  }else{
    osc = ctx.createOscillator()  
    osc.frequency.setValueAtTime(note, ctx.currentTime);
    osc.type = type
  }
  return [osc]
}

function pitchBend(ctx, osc, duration, p, q){
  let out;
  if(!osc.frequency){
    out = osc.playbackRate
  }else{
    out = osc.frequency
  }
  let newfreq = out.value*Math.pow(2, p-1)
  if(q==undefined || q==1){
    out.linearRampToValueAtTime(newfreq, ctx.currentTime+duration)
  }
  if(q<1){
    out.linearRampToValueAtTime(newfreq,ctx.currentTime+(duration*q))
  }else{
    let diff = (newfreq - out.value)/q
    let interval = duration/q
    for(i=0; i<q; i++){
      out.linearRampToValueAtTime(out.value+diff*i, ctx.currentTime+interval*(i+1))
    }
  }
}

function createWave(w){ // taken from tinysynth 
  const imag=new Float32Array(w.length)
  const real=new Float32Array(w.length)
  for(let i=1;i<w.length;++i)
    imag[i]=w[i]
  return ctx.createPeriodicWave(real,imag)
}

function noise0(){ // n0
  var blen = ctx.sampleRate
  let noiseBuf = ctx.createBuffer(1,blen,ctx.sampleRate)
  var dn= noiseBuf.getChannelData(0)
  for(let i=0;i<blen;++i){
    dn[i]=Math.random()*2-1
  }
  return new AudioBufferSourceNode(ctx, {buffer:noiseBuf})
}

function noise1(){ // n1
  var blen = ctx.sampleRate
  let noiseBuf = ctx.createBuffer(1,blen,ctx.sampleRate)
  var dr = noiseBuf.getChannelData(0)
  for(let jj=0;jj<64;++jj){
    const r1=Math.random()*10+1
    const r2=Math.random()*10+1
    for(let i=0;i<blen;++i){
      var dd=Math.sin((i/blen)*2*Math.PI*440*r1)*Math.sin((i/blen)*2*Math.PI*440*r2)
      dr[i]+=dd/8
    }
  }
  return new AudioBufferSourceNode(ctx, {buffer:noiseBuf})
}