let texts =
[
  `Ils changent leurs robes
  à chaque printemps
  pour couvrir leur passé
  Tapissent leurs faces
  de nouvelles gouaches
  pour se réinventer
  Ces monstres
  Partagent le même sang
  Disent leurs propres mythes
  et mangent leurs enfants
  <i>They change their dresses
  each spring
  to cover their past
  Upholster their faces
  with new paints
  to reinvent themselves
  These monsters
  Are sharing the same blood
  They tell their own myths
  and eat their children</i>`,
  
  `Retournons le miroir,
  Les coupables sont eux
  Retournons le miroir
  <i>Let’s turn the mirror over
  They are the culprits
  Let’s turn the mirror over</i>`,
  
  `Jusqu’à les épuiser
  Ils volent travail, temps, terres
  Et ne valent pas plus
  Que leur fausse monnaie
  Les chemins sont brouillés
  et à travers les strates
  ils se font oublier
  qu’on les marque aux fers
  Ils pourrissent nos terres
  <i>Until they are exhausted
  They steal work, time, land
  And don't worth much more
  Than their fake currencies
  The paths are blurred
  and through the strata
  They lie low
  Let’s brand them with iron
  They are ruining our land</i>`,
  
  `Retournons le miroir,
  Les coupables sont eux
  Retournons le miroir
  <i>Let’s turn the mirror over
  They are the culprits
  Let’s turn the mirror over</i>`,

  `Aujourd’hui se tiendra
  leur condamnation
  enfin, qui les pendra
  Par leurs stock-options
  Nous sommes ceux qu’ils ont 
  Trompés, vendus, volés
  installons leur procès
  Sur leurs cendres, semons
  <i>Today will be held
  their condemnation
  that will hang them, finally
  by their stock-options
  We are the ones they have
  Swindled, sold, stolen
  Let's set up their trials
  On their ashes, let’s sow</i>`,
  
  `Retournons le miroir,
  Les coupables sont eux
  Retournons le miroir
  <i>Let’s turn the mirror over
  They are the culprits
  Let’s turn the mirror over</i>`,
  
  ''
]

var link = document.createElement('link');
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = 'addons/lyrics/lyrics.css';
d.head.appendChild(link);
let lyricsBox = d.createElement("div")
lyricsBox.classList.add("lyrics-box")
b.appendChild(lyricsBox)

let lyrics_count = 0

function l(next=true){
  if (!next) {
    lyricsBox.innerHTML = ""  
    lyrics_count = 0  
  }else{
    if (lyrics_count == texts.length) {
      lyrics_count = 0
    }
    lyricsBox.innerHTML = texts[lyrics_count]
    lyrics_count++
  }
}

