let cmView
(function (exports) {
  'use strict';

  const {EditorView} = CM["@codemirror/view"];

  // Folding thxxx @borsTiHD!!!
  const {foldService} = CM ['@codemirror/language'];
  const foldingOnIndent = foldService.of((state, from, to) => {
    const line = state.doc.lineAt(from)
    const lines = state.doc.lines
    const indent = line.text.search(/\S|$/)
    let foldStart = from 
    let foldEnd = to 
    let nextLine = line
    while (nextLine.number < lines) {
      nextLine = state.doc.line(nextLine.number + 1)
      const nextIndent = nextLine.text.search(/\S|$/) 
      if (nextIndent > indent) {
        foldEnd = nextLine.to 
      } else { break }
    }
    if (state.doc.lineAt(foldStart).number === state.doc.lineAt(foldEnd).number) {
      return null
    }
    foldStart = line.to
    return { from: foldStart, to: foldEnd }
  })

  //!facet
  const {Facet} = CM["@codemirror/state"];
  const stepSize = Facet.define({
    combine: values => values.length ? Math.min(...values) : 2
  });

  // tagTheLines plugin
  function tagLines(options = {}) {
    return [tagTheLines]
  }

  // addDataToLines
  const {Decoration} = CM["@codemirror/view"];
  const {RangeSetBuilder} = CM["@codemirror/state"];

  function addDataToLines(view) {
    let step = view.state.facet(stepSize)
    let builder = new RangeSetBuilder();
    for (let {from, to} of view.visibleRanges) {
      for (let pos = from; pos <= to;) {
        let line = view.state.doc.lineAt(pos);
        let lineTag = line.text.split(":")
        if (lineTag != null) {
          // remove space character
          let dkey =  lineTag[0].replace(/\s/g, "");
          let dval = 0
          if(lineTag[1])dval = lineTag[1].trim()
          // removes "-" only if id
          if(dkey == "-id") dkey = dkey.replace('-','')
          builder.add(line.from, line.from, Decoration.line({
            attributes: {"data-key":dkey, "data-val":dval, "data-nbr":line.number, "style":"--line:"+line.number+";"}
          }));
        }
        pos = line.to + 1;
      }
    }
    return builder.finish()
  }

  // tagTheLines Plugin
  const {ViewPlugin} = CM["@codemirror/view"];
  const tagTheLines = ViewPlugin.fromClass(class { 
    constructor(view) {
      this.decorations = addDataToLines(view);
    }
    update(update) {
    if (update.docChanged || update.viewportChanged)
      this.decorations = addDataToLines(update.view);
    }
  }, {
    decorations: v => v.decorations
  });

  // auto completion
  const {snippetCompletion} = CM["@codemirror/autocomplete"]
  // const {javascript, javascriptLanguage} = CM["@codemirror/lang-javascript"]
  const {json, jsonLanguage} = CM["@codemirror/lang-json"]
  const{ StreamLanguage } = CM['@codemirror/language']
  // const {yaml} = CM["@codemirror/legacy-modes/mode/yaml"]
  // import * as yamlMode from "@codemirror/legacy-modes/mode/yaml";
  // import parser from "js-yaml";

  const {basicSetup} = CM["codemirror"];
  const {keymap} = CM["@codemirror/view"];
  const {indentWithTab} = CM["@codemirror/commands"];

  cmView = new EditorView({
    doc:"",
    lineWrapping: true,
    extensions: [
      basicSetup,
      EditorView.lineWrapping,
      // StreamLanguage.define(yaml),
      foldingOnIndent,
      tagLines(),
      keymap.of([indentWithTab]),
      json(),
      jsonLanguage.data.of({
        autocomplete: [
          // snippetCompletion('m(${ids},${key},${val})', {label: 'modify'}),
          snippetCompletion('cl(${ids})', {label: 'clone'}),
          snippetCompletion('solve($ % ${value} * 200)', {label: 'solve'}),
          snippetCompletion('r(${min},${max})', {label: 'random'}),
          snippetCompletion('sh(${list})', {label: 'shuffle'}),
          // snippetCompletion('rm(${ids})', {label: 'remove'}),
          snippetCompletion('eucl(${beats},${steps},${val1},${val2},$)', {label: 'euclidean'}),
          snippetCompletion('gl(${beats},${duration},${silence},${rand},$)', {label: 'glob'}),
          snippetCompletion('beads(${-.},${val1},${val2},$)', {label: 'beads'}),          
          snippetCompletion('arc(${nbrItem},${max},${min},$)', {label: 'arc'}),          
          snippetCompletion('re(${occurence},${toRepeat})', {label: 'repeat'}),
          snippetCompletion('changeAll(${key},${val})', {label: 'changeAll'}),
          snippetCompletion('expo(10,1)', {label: 'kick'}),
          snippetCompletion('del(.2,2,1)', {label: 'delay'}),
          snippetCompletion('rev(.2,2)', {label: 'reverb'}),
          snippetCompletion('stop()', {label: 'stop'}),
          snippetCompletion('start()', {label: 'start'}),
          snippetCompletion('sine,sawtooth,noise,square,triangle,onoise', {label: 'synthlist'}),
          // snippetCompletion('itv', {label: 'itv'}),
          snippetCompletion('resetStyle()', {label: 'resetStyle'}),
          snippetCompletion('resetCycles()', {label: 'resetCycles'}),
          snippetCompletion('style("body{background:cyan;}")', {label: 'style'}),
          snippetCompletion('style(".editor{text-align:center;}")', {label: 'style:center'}),
          snippetCompletion('style(".editor{animation:b 10s linear alternate-reverse infinite;} @keyframes b{0% {filter:blur(10px);}100%{filter:blur(0);}}")', {label: 'styleBlur'}),
          snippetCompletion('style(".editor{animation:h 10s linear alternate-reverse infinite;} @keyframes h{0% {filter:hue-rotate(0);}100%{filter:hue-rotate(180deg);}}")', {label: 'styleHue'}),
          snippetCompletion('style(".cm-line{animation:l 1.8s ease-in-out calc(var(--line)*.05s) alternate-reverse infinite;} @keyframes l{0% {rotate:0deg;}100%{rotate:-2deg;}}")', {label: 'lineDance'}),
          snippetCompletion('newPattern()', {label: 'newPattern'}),
          snippetCompletion('B2, E3, F3, G3, G#3, C4, F4, G4, G#4', {label: 'mel:myst'}),
          snippetCompletion('re(16,sh(C#5,E5,G#5,C#6)),re(16,sh(E5,G#5,B5,E6)),re(16,sh(D5,F#5,A5,D6)),re(16,sh(F#5,A5,C#6,F#6))', {label: 'mel:phrygianTrip'}),          
          snippetCompletion('200,300,240,220,470,810,130,70', {label: 'mel:minorFlowers'}),
          snippetCompletion('G5,D#5,A#4,G#4,G4,D#4,G3,D#3,A#2,G2,D#2', {label: 'mel:happyCow'}),
          snippetCompletion('C5, E5, B4, E4, E2', {label: 'mel:labStories'}),
          snippetCompletion('D5,C#5,D#5,G5,F#5,B5,C6,D6,C#6,D#6,G6,D#4,G4,G3,F#3,B3,C3', {label: 'mel:snakeglow'}),
          snippetCompletion('B3_F#4_E4, B3_E3_C4,B3_F#4_E4, B3_E4_G#4', {label: 'mel:monstertune'}),
          snippetCompletion('G3,A3,C3,D3,E3,G3,A3,C4,D4,E4,G4,A4,C5,D5,E5', {label: 'mel:maPenta'}),
          snippetCompletion('G#3,B3,C#4,E4,F#4,G4,A4,G#4,B4,C#5,E5,F#5', {label: 'mel:maPentaAlt'}),
          snippetCompletion('G#3_C4_D#4,C3_D#4_G4,F3_G#4_C4,A#3_C#4_F4,D#3_G4_A#4,C3_D#4_G4,F3_G#4_C4,C#3_F4_G#4', {label: 'chord:G#Ionian'}),
          // snippetCompletion('load("sheets/${name}.yaml")', {label: 'load()'}),
          snippetCompletion('loadAddon("${name}")', {label: 'loadAddon()'}),
          snippetCompletion('theme("${dark}")', {label: 'theme()'}),
          snippetCompletion('every(${cycleNbr},${val1},${val2},$)', {label: 'every()'}),
          snippetCompletion('sometimesby(${chances},${val1},${val2})', {label: 'sometimesby()'}),
          snippetCompletion('always(${val1},${val2})',{label:'always'}),
          snippetCompletion('almostalways(${val1},${val2})',{label:'almostalways'}),
          snippetCompletion('often(${val1},${val2})',{label:'often'}),
          snippetCompletion('sometimes(${val1},${val2})',{label:'sometimes'}),
          snippetCompletion('rarely(${val1},${val2})',{label:'rarely'}),
          snippetCompletion('almostnever(${val1},${val2})',{label:'almostnever'}),
          snippetCompletion('never(${val1},${val2})',{label:'never'}),
          snippetCompletion('G3_A#4_D4,0,G4_C3_D#4,0',{label:'chords:hello-ete'}),
          snippetCompletion('newPattern(["trig:itv/8","note:r(1200,1300)","synth.gain:.15","synth.dur:sh(.04,.2)","synth.env:0,1,0"])',{label:'pat:whistl'}),
          snippetCompletion('newPattern(["trig:gl(5,1.8,8,1,$)","note:sh(G3,A3,C3,D3,E3,G3,A3,C4,D4,E4,G4,A4,C5,D5,E5)","synth.gain:.15","synth.dur:sh(.5,1.5)","synth.env:0.5,1,1,0.3,0"])',{label:'pat:5yearsOld'}),
          snippetCompletion('newPattern(["trig:itv/1,itv/2,itv/4","note:C2","synth.gain:sometimesby(.25,.7,0)","synth.dur:1.2,.1,.2"])',{label:'pat:dundun'}),
          snippetCompletion('newPattern(["trig:itv/8","note:sometimesby(.5,0,240)","synth.gain:beads(----------..-...----.----.--..--,0,sh(.3,1),$)","synth.dur:.2","synth.fx:expo(10,1)"])',{label:'pat:dundun2'}),
          snippetCompletion('newPattern(["trig:itv/4","note:140","synth.pan:0","synth.type:sine","synth.gain:sometimesby(.55,.7,0)","synth.dur:.4,.2,sh(1,.1),0","synth.fx:expo(7,1)","synth.env:1,0"])',{label:'pat:droneperc'}),
          snippetCompletion('newPattern(["trig:gl(7,1,5,1,$)","note:E3,F3","pan:-.8","synth.gain:.15","synth.dur:sh(.1,.5,1)","synth.env:1,0","synth.type:sh(square,triangle,noise,onoise)"])',{label:'pat:grape'}),
          snippetCompletion('newPattern(["trig:gl(3,itv,itv*3,0,$)","note:E4,G3,F4,re(3,E4)","pan:-.8","synth.gain:.15","synth.dur:sh(.1,.5,1)","synth.env:1,0","synth.type:sh(square,triangle,noise)"])',{label:'pat:trigrape0'}),
          snippetCompletion('newPattern(["trig:itv/6","note:2300","synth.gain:eucl(sh(3,7),9,.2,sh(.1,0),$)","synth.dur:sh(0.4,.1)"])',{label:'pat:tern'}),
        ]
      })
    ],
    parent: document.querySelector('.editor'),
  })
  exports.tagLines = tagLines;
  Object.defineProperty(exports, '__esModule', { value: true });
  return exports;
})({});

async function getInitialSheet(path){
  return new Promise((resolve) => {
    fetch(path, {cache: "no-store"})
      .then(response => response.text())
      .then((sheet) => {
        resolve(sheet)
      })
  })
}

// URI encoding / decoding

// Init by decoding the URI, if it has hash
if (document.location.href.split('#')[1]) {
  decode()
  // check if a file has to be loaded in URL after ?
}else if(document.location.href.split('?')[1]){
  let file = document.location.href.split('?')[1]
  load('sheets/'+file)
  // check localstorage, if a sheet exists, load it
}else if(localStorage.getItem("ete")){
  fillEditor(localStorage.getItem("ete"))
}else{
  // or load default sheet
  load("sheets/hello-ete.yaml")
}

async function load(sheetPath){
  let sheet = await getInitialSheet(sheetPath)
  fillEditor(sheet)
}

function fillEditor(sheet){
  cmView.dispatch({
    changes:{
      from:0,
      to:cmView.state.doc.length,
      insert:sheet
    }
  })
}

// Text to URI and preview
function encode(){
  html_text = cmView.state.doc.toString()
  let encoded = Base64.encode(html_text);
  let currentURI = document.location.href;
  let newURI = currentURI
  // if a file is lodaded with /?file.yaml, don’t write b64 in URI
  if (!currentURI.includes("?")) {
    newURI = newURI.split('#')[0]+"#"+encoded
    document.location.href = newURI
  }
  return encoded
}


// URI to editor
function decode(hash=false){
  let encoded
  let uri = document.location.href;
  if(hash){
    encoded = hash
  }else{
    encoded = uri.split('#')[1]
  }
  if (encoded == undefined || encoded == "" || uri == "#") {
    // do nothing
  }else{
    fillEditor(Base64.decode(encoded))
  }
}
