var cssLink = document.createElement('link');
cssLink.rel = 'stylesheet';
cssLink.type = 'text/css';
cssLink.href = 'addons/pdv1/pdv1.css';
d.head.appendChild(cssLink);
b.dataset.theme = "bright"
let pdv1Started = false
let svgTimeOut
function pdv1(){
  if (pdv1Started) {
    pdv1Started = false
  }else{
    d.addEventListener("percsynth", (e) => {
      generateSVG()
    })
    d.addEventListener("playpattern", (e) => {
      generateSVG()
    })
    pdv1Started = true
  }
}
pdv1()

let j = 0
let svgTag = `<svg class='svg-lines' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'></svg>`
b.insertAdjacentHTML("beforeend",svgTag)
let svg = d.querySelector('body svg')
function generateSVG() {
  clearTimeout(svgTimeOut)
  let html = `<rect x0='0' y0='0' width='100' height='100' fill='transparent' stroke='none'></rect>`;
  for (let i = 1; i > 0; i--) {
      const x0 = Math.random() * 100;
      const y0 = Math.random() * 100;
      const x1 = Math.random() * 100;
      const y1 = Math.random() * 100;
      const x2 = Math.random() * 100;
      const y2 = Math.random() * 100;
      const a = x2-x1;
      const b = y2-y1;
      const strokeWidth = 2;
      const color = 'blue';
      html += `<path fill='none' stroke='${color}' stroke-width='${strokeWidth}' stroke-linejoin='round' stroke-linecap='round' d='M ${x0} ${y0} Q ${x1-b}  ${y1-a} ${x1} ${y1} Q ${x1+b} ${y1+a} ${x2} ${y2}'/>`;
      // html += `<circle cx='${x0}' cy='${y0}' r='${3 * strokeWidth}' fill='${color}'></circle>`;
      j++
  }
  svg.innerHTML = html;
  svgTimeOut = setTimeout(() => {
    svg.innerHTML = "";      
  }, 20000);
}
