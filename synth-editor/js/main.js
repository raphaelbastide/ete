let d = document
let patchLine = d.querySelector('.patch-line')
let patchLineEte = d.querySelector('.patch-line-ete')
let currentSynth = {note:200, type:patchLine.innerHTML, gain: 1, dur: 0.5, fx:0, env:"1,0"}

init()
hasMidi = false
function init() {

  patchLine.innerHTML = JSON.stringify(Object.values(programs)[0]["patch"])
  for (let programName in programs) {
    // let d = programs[programName];
    let btn = d.createElement('button')
    btn.innerHTML = programName;
    btn.addEventListener("mousedown", function () {
      let patch = programs[programName]["patch"];
      patch = JSON.stringify(patch)
      patchLine.innerHTML = patch
      edit()
      shot()
    })
    btn.addEventListener("mouseup", function () {
    })
    d.querySelector(".synthlist").appendChild(btn);
  }
  d.querySelector(".shot").addEventListener("mousedown",function(){
    shot()
  })
  d.addEventListener("keydown", function (event) {
    if (event.code === "Space") {
      event.preventDefault()
    shot()
    }
  })
  patchLine.addEventListener('keydown', function(event){
    edit()  
  })
}

function shot(){
  let patch = patchLine.innerHTML
  // let w = {name:synthName,p:patch}
  // avoid (replace) comma to pass the fm synth
  updateLines(patch)
  patch = patch.replace(/"/g, "'").replace(/,/g,'œ')
  currentSynth.type = patch
  playNote(400,currentSynth,0,0)
}
function edit(){
  currentSynth.type = patchLine.innerHTML
}
function updateLines(patch){
  patchLine.innerHTML = patch
  patchLineEte.innerHTML = "fm("+patch+")"
}