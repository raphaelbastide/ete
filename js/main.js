let d = document
let b = d.body
const editor = d.querySelector('.editor')
const messageBox = d.querySelector('.message-box')
const patternBox = d.querySelector('.pattern-box')
let version = 1.9
let sheet = {}
let dynaSheet = {}
let initTime, onRecPattern, lastPatternID
let id = 0
let fingerCycle = 0
let started = false
let hasSolo = false
let hasMidi
let midiOutput
let midiInputs

init()

async function init(){
  return new Promise((resolve) => {
    sheet.config = new Config()
    sheet.patterns = []
    dynaSheet.patterns = []
    initTime = Date.now()
    editor.addEventListener('keydown', handleKeyDown);
    editor.addEventListener('keyup', handleKeyUp);
    console.log('Welcome to été');
    d.querySelector('.version').innerText = version
    // Enable WebMidi.js
    // /!\ use localhost, not dev.ete, in firefox nightly
    // Midi note number = ete pattern id
    WebMidi.enable((err) => {
      if (err) {
        console.log('No webMIDI (unsupported)')
        console.log('WebMidi could not be enabled.', err)
        hasMidi = false
        resolve('init-done')
      } else {
        midiOutput = WebMidi.outputs[1]
        // device = listPickDevice("UMC204HD")
        // midiInputs = [["microKONTROL _ CTR",6],["UMC204HD",10]]
        midiInputs = [["MIDIMATE",10]]
        midiInputs.forEach(midiInput => {
          let name = midiInput[0]
          let channel = midiInput[1]
          device = listPickDevice(name)
          midiInput = WebMidi.getInputByName(device)
          if(midiInput == false) message('Invalid MIDI input', 'error')
          midiInput.addListener("noteon",channel, (e) => {
            sheet.patterns.forEach(pattern => {
              if (pattern.id == e.note.number && pattern.state == "m-in") {
                // store original value to set it back after
                let s = pattern.synth
                let cycle = pattern.meta[0]
                let vel = e.velocity
                let originalGain = s.gain
                let originalDur = s.dur
                let originalNote = pattern.note
                // store original value to set it back after
                let newGain = solve(s.gain, cycle, false, vel)
                let newDur = solve(s.dur, cycle, false, vel)
                let newNote = solve(pattern.note, cycle, false, vel)                
                s.gain = newGain
                s.dur  = newDur
                pattern.note  = newNote
                // security
                if (s.gain > 1) s.gain = 1 
                startPattern(pattern)
                // set gain back to orinigal value for next beat
                // s.gain = originalGain
                s.dur = originalDur
                s.gain = originalGain
                pattern.note = originalNote
              }
            })
          });
          d.querySelector('footer').insertAdjacentHTML('beforeend','<p>M</p>').
          hasMidi = true
        })
        resetCycles()
      }
    })
    // Onboarding message
    message('Click on play or ctrl+r', 'log',8000)
    resolve('init-done')
  })
}

function listPickDevice(string){
  for (var i = 0; i < WebMidi.outputs.length; i++) {
    console.log(WebMidi.outputs[i].name, WebMidi.outputs[i].id, WebMidi.outputs[i])
    if (WebMidi.outputs[i].name.includes(string)) {
      console.log('MIDI OUTPUT CHOSEN ↑');
      return WebMidi.outputs[i].name
    }
  }
}

function Config(){
  this.comment = "Made with été v"+version
  this.percsynth = {note:200, type:'sine', gain: 0.5, dur: 0.1, fx:0, env:"1,0"}
  this.itv = null
  this.exe = null
  this.mode = "perc" // rec, type, or perc
}

function Pattern(startTime){
  this.id = getID()
  this.meta = [0,0,1] // patternCycle, beatCycle, firstMidiBeat
  this.state = "p"
  this.trig = 0
  this.note = 200
  this.pan = 0
  this.synth = []
  this.beats = []
}

function KeyPressed(startTime){
  this.startTime = startTime
  this.id = getID()
  this.offset = 0
}

function handleKeyDown(e) {
  let mode = sheet.config.mode
  // Check if the key pressed is within the allowed range and mode is 'type' or 'perc'
  if ((mode === 'type' || mode === 'perc') && ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90))) {
    // Check for Ctrl + S shortcut
    if (e.ctrlKey && e.key === 's') {
      // do nothing for save shortcut
    } else {
      // Perform actions based on mode and key combinations
      if (mode === 'type' || (mode === 'perc' && e.altKey)) {
        doType(e);
      }
      if (mode === 'perc' && e.altKey) {
        // Percsynth event
        const percSynth = new CustomEvent("percsynth", {'detail': e})
        d.dispatchEvent(percSynth)
      }
      // Check if the percsynth line is in view and update its style
      const percsynthLine = editor.querySelector('[data-key="percsynth"]');
      if (percsynthLine) {
        percsynthLine.style.setProperty('--linecolor', 3 * 52);
        percsynthLine.classList.add('playing');
        setTimeout(() => {
          percsynthLine.classList.remove('playing');
        }, 30);
      }
    }
  }
  // Check for Alt key press
  if (e.keyCode === 18) {
    e.preventDefault()
    if (mode === 'rec') {
      onRecPattern = newPattern('', false)
      onRecPattern.startTime = Date.now()
    }
  } else if (e.ctrlKey && e.key === 's') {
    // CTRL + S = save
    e.preventDefault()
    saveEditor()
  } else if (e.ctrlKey && e.key === 'r') {
    // CTRL + R = start / stop
    e.preventDefault()
    started ? stop() : start()
  }
  // Handle Alt key press
  if (e.altKey) {
    e.preventDefault()
    recKey(e)
    if (!started) {
      console.log('started')
      d.querySelector('[data-btn="start"]').innerText = '‖'
      started = true
    }
  }
  // Manual mode, check for altgr to trigger pattern
  if (e.key === "AltGraph" && mode === "manual") {
    e.preventDefault()
    sheet.patterns.forEach(pattern => {
      startPattern(pattern)
    });
  }
  // Dispatch custom keyDown event
  const keyDownEvent = new CustomEvent('keyDown', {'detail': e})
  d.dispatchEvent(keyDownEvent)
}

function handleKeyUp(e){
  // if key up is ALT
  if (e.keyCode === 18) {
    e.preventDefault()
    if (sheet.config.mode == "rec") {
      onRecPattern.endTime = Date.now()
      // if itv is set
      if (typeof sheet.config.itv == 'number') {
        try {
          onRecPattern.trig = (onRecPattern.endTime - onRecPattern.startTime) / 1000
          // tries to stick pattern trigger to the closer refTrig
          let refTrig = sheet.config.itv
          // The new pattern trigger is the rounded result of the pattern.dur / ref.dur
          let divi = onRecPattern.trig / refTrig
          let alignFactor
          if (divi >= 1) {
            alignFactor = Math.round(divi)
          // < 1? —> new pattern is shorter, align it to cool decimals
          }else{
            alignFactor = Math.round(divi * 10) / 10
          }
          onRecPattern.trig = Math.round(refTrig * alignFactor)
          onRecPattern.trig = "itv * "+onRecPattern.trig / sheet.config.itv
        }catch(error){
          console.error(error);
        }
      // if itv not set
      }else{
        // set itv to the first pattern’s trig
        sheet.config.itv = onRecPattern.endTime - onRecPattern.startTime
        onRecPattern.trig = "itv * 1"
      }
      // if has recorded beats:
      if (onRecPattern.beats.length > 0) {
        onRecPattern.beats.forEach(beat => {
          // offset to %
          beat.offset = Number(((beat.startTime - onRecPattern.startTime) * 100 / solve(onRecPattern.trig,fingerCycle, false))) / 1000
          delete beat.startTime
        });
        // remove startTime and endTime because we don’t care anymore
        delete onRecPattern.startTime
        delete onRecPattern.endTime
        startPattern(onRecPattern)
        lastPatternID = onRecPattern.id
        onRecPnRecPattern = false
      // if pattern is empty:
      }else{
        removePatternById(onRecPattern.id)
      }
      updateHTMLlog(sheet)
    }
  }
  // if key up is not ALT but with ALT pressed:
  if (e.altKey) {
    e.preventDefault()
  }
}

function recKey(e){
  // not alt key, and rec mode
  if (e.keyCode != 18 && sheet.config.mode == "rec") {
    let k = new KeyPressed(Date.now())
    // clone synth for the pattern currently on record:
    onRecPattern.synth = {...sheet.config.percsynth,
      note: sheet.config.percsynth.note,
      type: sheet.config.percsynth.type,
      dur:  sheet.config.percsynth.dur,
      gain: sheet.config.percsynth.gain,
      fx:  sheet.config.percsynth.fx,
      env:  sheet.config.percsynth.env
    }
    // config note mapped to keycode:
    if(sheet.config.percsynth.note == "key"){
      note = e.keyCode * 10
      k.note = e.keyCode * 10
    }else{ // config note is set to a value
      // not stored, only use for sound
      note = onRecPattern.synth.note
    }
    // we don’t need synth.note, used only for live typing, but we need a pattern note
    delete onRecPattern.synth.note
    onRecPattern.note = note
    // rec mode, add patterns to pattern list
    onRecPattern.beats.push(k)
    // play note for feedback only, based on sheet.config.percsynth
    playNote(solve(note,fingerCycle, false), sheet.config.percsynth, sheet.config.pan)
    fingerCycle++
  }
}

function doType(e){
  // type mode, no recording
  let solved = {...sheet.config.percsynth,
    note: solve(sheet.config.percsynth.note, fingerCycle, false),
    type: solve(sheet.config.percsynth.type, fingerCycle, false),
    dur:  solve(sheet.config.percsynth.dur,  fingerCycle, false),
    gain: solve(sheet.config.percsynth.gain, fingerCycle, false),
    // we want to keep arrays type here:
    fx:   solve(sheet.config.percsynth.fx,  fingerCycle, false),
    env:  solve(sheet.config.percsynth.env,  fingerCycle, true)
  }
  // note mapped to keycode
  if(sheet.config.percsynth.note == "key"){
    note = e.keyCode * 10
  }else{ // not mapped to keycode
    note = solved.note
  }
  playNote(note, solved, sheet.config.pan)
  fingerCycle++
}

function startPattern(pattern){
  if ((sheet.config.mode == "manual" || pattern.state == "m-in")) {
    // pattern.meta[2] is firstMidiBeats
    if ( pattern.meta[2] == 1) { // avoid playing all firstMidiBeats
      pattern.meta[2] = 0; return
    }else{
      playPattern(pattern)
    }
    
  }else{    
    playPattern(pattern)
    setLoop(pattern)
    // set firstMidibeat back to true to avoid future firstMidiBeats sounds
    pattern.meta[2] = 1
  }
  pattern.meta[0]++ // first incrementation
}

function setLoop(pattern){  
  let singleCycle = function(){    
    let foundPattern = searchKeyVal(sheet, 'id', pattern.id)
    if (foundPattern !== pattern) {
      pattern = foundPattern
    }
    playPattern(foundPattern)
    // looping incrementation
    pattern.meta[0]++
  }

  // Dynamic sheet is used to store the loop, as we can’t have it dynamic in json / yaml sheet
  dynaSheet.patterns.push({
    id:pattern.id,
    loop:new AdjustingInterval(singleCycle, pattern.id)
  })

  let dynaPattern = searchKeyVal(dynaSheet, 'id', pattern.id)
  dynaPattern.loop.start()
}

// The following function avoid time drifts of setTimeouts
function AdjustingInterval(cycleFunction, id) {
  let that = this;
  let pattern = searchKeyVal(sheet, 'id', id)
  let interval = Number(solve(pattern.trig, pattern.meta[0], false) * 1000)
  let expected, timeout;
  this.interval = interval;
  this.start = function() {
    expected = Date.now() + this.interval;
    timeout = setTimeout(step, this.interval);
  }
  this.stop = function() {
    clearTimeout(timeout);
  }
  function step() {
    let pattern = searchKeyVal(sheet, 'id', id)
    if (pattern) {
      // interval update
      that.interval = Number(solve(pattern.trig, pattern.meta[0], false) * 1000)
      let drift = Date.now() - expected;
      cycleFunction();
      expected += that.interval;
      timeout = setTimeout(step, Math.max(0, that.interval-drift));
    }
  }
}

function playPattern(pattern){  
  // do not play if is muted or another one is solo (has ×)
  if (typeof pattern.state !== 'undefined' ) {
    if (pattern.state == "m" || pattern.state.includes("×")) return
    // a pattern is solo
    if (pattern.state == "s") solo(pattern.id)
  }
  // check if a pattern has been unsolo
  if (hasSolo) unSolo()
  if (pattern.beats) {    
    pattern.beats.forEach(beat => {
      let cycle = pattern.meta[0]      
      // % to millisec offset time:
      offset = solve(beat.offset, cycle, false) * solve(pattern.trig, cycle, false) / 100 * 1000
      // Overwrite pattern keys only if it exists in the beat
      let note = beat.note  !== undefined ? beat.note : pattern.note
      let pan  = beat.pan   !== undefined ? beat.pan : pattern.pan
      let type = beat.synth !== undefined && beat.synth.type !== undefined ? beat.synth.type : pattern.synth.type
      let dur  = beat.synth !== undefined && beat.synth.dur  !== undefined ? beat.synth.dur : pattern.synth.dur
      let gain = beat.synth !== undefined && beat.synth.gain !== undefined ? beat.synth.gain : pattern.synth.gain
      let fx  = beat.synth !== undefined && beat.synth.fx  !== undefined ? beat.synth.fx : pattern.synth.fx
      let env  = beat.synth !== undefined && beat.synth.env  !== undefined ? beat.synth.env : pattern.synth.env
      // clone synth and modify with solved values
      const synth = {...pattern.synth,
        type: solve(type, cycle, false),
        dur:  solve(dur, cycle, false),
        gain: solve(gain, cycle, false),
        fx:  solve(fx, cycle, false),
        env:  solve(env, cycle, true)
      }
      setTimeout(() => {
        // do not play beat if été stopped        
        if (started && synth.dur!=0 && synth.gain!=0 && synth.type!=0) {
          let beatCycle = pattern.meta[1]          
          let solvedNote = solve(note, beatCycle, false)
          // if(solvedNote == 0) return          
          playNote(solvedNote, synth, solve(pan, beatCycle, false),pattern)
          showLine(beat)
          // triggers event
          const playPattern = new CustomEvent("playpattern", {'detail': beat.id})
          d.dispatchEvent(playPattern)
          // increment beat cycle only if gain, dur != 0, used to keep note incrementation for beads() for instance
          pattern.meta[1]++
        }        
        // // increment beat cycle even if gain, dur = 0
        // pattern.meta[1]++
      }, offset);
    });
  }
}

function updateHTMLlog(data){
  let content
  const doc = new YAML.Document(data, {aliasDuplicateObjects:false})
  // set the YAML paths to be kept as inline arrays
  YAML.visit(doc, {
    Pair(_, pair) {
      if (pair.key.value === 'env'
        || pair.key.value === 'trig'
        || pair.key.value === 'meta'
        || pair.key.value === 'offset'
        || pair.key.value === 'type'
        || pair.key.value === 'dur'
        || pair.key.value === 'pan'
        || pair.key.value === 'fx'
        || pair.key.value === 'note')
      return pair.value.flow = true;
    }
  })
  content = doc.toString()
  formatAndAppend(content)
}

function formatAndAppend(text){
  cmView.dispatch({
    changes:{
      from:0,
      to:cmView.state.doc.length,
      insert:text
    }
  })
}

function saveEditor(){
  let cursorPos = cmView.state.selection
  let text = cmView.state.doc.toString()
  messageBox.innerHTML = ""
  // empty editor
  if (text.length <= 1) {
    b.dataset.saved = 'saved-stop'
    stop()
  }else{
    text = getWildExe(text)
    let savedCycles = saveCycles(sheet)
    sheet = YAML.parse(text)
    // load previously saved cycles
    loadCycles(savedCycles, sheet)
    let exeCode = sheet.config.exe
    if (exeCode) {
      try{
        eval(exeCode)
        // avoid multiple exe
        sheet.config.exe = null
      }catch (error) {
        console.log(error)
        message(error,'error')
      }
      updateHTMLlog(sheet)
    }
    localStorage.setItem("ete", cmView.state.doc.toString())
    d.title = sheet.config.comment + " - été"
    b.dataset.saved ='saved'
    const saveEvent = new CustomEvent("save", {'detail': sheet})
    d.dispatchEvent(saveEvent)
    // retrive cusror position
    cmView.dispatch({selection: cursorPos}) 
    setTimeout(() => {
      b.dataset.saved = ""
    }, 100);
  }
}

function saveCycles(sheet){
  // TODO could be enhanced with ... spread obj? see playpattern synth duplicate, or a shadow sheet that hold dynamic values
  // save cycles before sheet update
  let obj = []
  sheet.patterns.forEach(p => {
    obj.push({id: p.id, cycle: p.meta})
  })
  return obj
}

function loadCycles(tempCycles, sheet){
  tempCycles.forEach(c => {
    let p = searchKeyVal(sheet, 'id', c.id)
    if (p){
      p.meta = c.cycle
    }
  })
}

function searchKeyVal(obj, keyName, valName=undefined) {
  for (prop in obj) {
    if (typeof obj[prop] == "object") {
      let objectFound
      if (valName !== undefined) { // searchKey mode
         objectFound = searchKeyVal(obj[prop], keyName, valName);
      }else{
         objectFound = searchKeyVal(obj[prop], keyName);
      }
      if (objectFound !== undefined) return objectFound
    }else{
      if (valName !== undefined) { // searchKey mode
        if (prop == keyName && obj[prop] == valName) {
          return obj
        }
      }else{
        if (prop == keyName) {
          return obj
        }
      }
    }
  }
}

function getWildExe(text){
  // find and eval wild exe functions in code, starting with !
  let wildExe = text.match(/(![A-Za-z]*\((\S*)\))/g)
  let cleanText = text
  if (wildExe != null) {
    wildExe.forEach(exeCode => {
      cleanText = text.replace(exeCode, '')
      exeCode = exeCode.replace('!','')
      eval(exeCode)
      updateHTMLlog(sheet)
    })
  }
  return cleanText
}

function removePatternById(patternID) {
  // doesn’t work
  // var result = sheet.patterns.filter(pattern => {
  //   return pattern.id === patternID
  // })
  // let index = sheet.patterns.indexOf(result)
  // delete index
  // console.log(sheet.patterns, dynaSheet.patterns);
  // updateHTMLlog(sheet)
}

// User functions

function stop(){
  dynaSheet.patterns.forEach(pattern => {
    pattern.loop.stop()
  });
  resetCycles()
  console.log('stopped');
  d.querySelector('[data-btn="start"]').innerText = "▶"
  // if(ctx)  ctx.close()
  started = false
  const stopEvent = new CustomEvent("stop");
  d.dispatchEvent(stopEvent);
}

function start(){
  if(started) return
  started = true
  saveEditor()
  sheet.patterns.forEach(pattern => {
    startPattern(pattern)
  });
  // set the id base to existing ids
  id = updateIds()
  d.querySelector('[data-btn="start"]').innerText = "‖"
  console.log('started');
  const startEvent = new CustomEvent("start");
  d.dispatchEvent(startEvent);
}

function reset(){
  load('sheets/default.yaml')
}

function newPattern(rules,empty = true){
  // use with rules newPattern('state:m,synth.type:square')
  let newPattern = new Pattern(Date.now())
  // empty is being use for some in-été functions
  if (empty) {
    newPattern.synth = {...sheet.config.percsynth}
    // Adjust trig to itv
    newPattern.trig = sheet.config.itv
    newPattern.trig = "itv * "+newPattern.trig / sheet.config.itv
    newPattern.beats = [{"id": getID(),"offset": 0}]
    delete newPattern.synth.note
  }
  if (rules) {
    if (rules.constructor != Array) {
      rules = [rules]
    }
    rules.forEach(rule => {
      rule = rule.split(" ").join("").split(":")
      let key = rule[0]
      let val = rule[1]
      // convert dot notation to obj reference
      dotStringToObj(newPattern,key,val)
    });
  }
  // appends to sheet’s begining
  sheet.patterns.unshift(newPattern)
  if(empty)startPattern(newPattern)
  return newPattern
}

function updateIds(){
  let ids = []
  sheet.patterns.forEach(pattern => {
    ids.push(pattern.id)
    pattern.beats.forEach(beat => {
      ids.push(beat.id)
    });
  })
  return Math.max(...ids) + 1
}

// modify
function m(ids, key, val){
  if(ids == "all"){
    sheet.patterns.forEach(pattern => {
      pattern[key] = val
    });
    return
  }else if(!Array.isArray(ids)){
    ids = [ids]
  }
  ids.forEach(id => {
    let objWithID = searchKeyVal(sheet, 'id', id)
    let objWithKey = searchKeyVal(objWithID, key)
    objWithKey[key] = val
  });
}

// remove
function rm(ids){
  if (!Array.isArray(ids)) {ids = [ids]}
  ids.forEach(id => {
    removePatternById(id)
  });
}

// clone
function cl(ids, mute=false){
  if (!Array.isArray(ids)) {ids = [ids]}
  ids.forEach(id => {
    let objWithID = searchKeyVal(sheet, 'id', id)
    const clone = JSON.parse(JSON.stringify(objWithID)) // see deep cloning
    clone.id = getID()
    if(mute) clone.state = "m"
    clone.beats.forEach(beat => {
      beat.id = getID()
    });
    sheet.patterns.unshift(clone)
    startPattern(clone)
  });
  updateHTMLlog()
}

// random
function r(min,max){
  // allow mistakes like r(1)
  if (min == undefined || max == undefined) return .5
  return (Math.random() * (max - min)) + min
}

// shuffle: random value in list (array)
function sh(){
  // there is an undefined number of arguments
  let list = Array.from(arguments);
  let val = list[Math.floor(Math.random() * list.length)];
  return val
}

// repeat, same as using ":" but more nestable
function re(repetitions, value){
  if (arguments.length > 2) {
    // case of multiple values
    let multipleValues = []
    // stats at 1, ignore first argument, others added to an array
    for (let i = 1; i < arguments.length; i++) {
      multipleValues.push(arguments[i])
    }
    value = multipleValues.join(',')
  }
  return Array.from({length: repetitions}, () => value).join(',')
}

// Time repartition functions
function sometimesby(chances, val1, val2=0){
  // A base for sometimes-like functions bellow, stolen from tidal cycles
  let array = []
  let stepNbr = 10
  for (let i = 0; i < stepNbr; i++) {
    if (Math.random() <= chances) {
      array.push(val1)
    }else{
      array.push(val2)
    }
  }
  return array;
}
function always(val1, val2){return sometimesby(1, val1, val2)}
function almostalways(val1, val2){return sometimesby(.9, val1, val2)}
function often(val1, val2){return sometimesby(.75, val1, val2)}
function sometimes(val1, val2){return sometimesby(.5, val1, val2)}
function rarely(val1, val2){return sometimesby(.25, val1, val2)}
function almostnever(val1, val2){return sometimesby(.1, val1, val2)}
function never(val1, val2){return sometimesby(0, val1, val2)}

function every(nbrCycle, val1, val2=0, cycle){
  if (cycle % nbrCycle == 0) {return val1}
  else {return val2}
}

function beads(){
  let args = Array.from(arguments)
  let pattern = args[0]
  let cycle = args.at(-1)
  values = args
  // removes first and last arguments (pattern and cycle)
  args.shift()
  args.pop()
  values = args
  let step = pattern[cycle % pattern.length]
  let uniqueVals = Array.from(new Set(pattern))
  // return the value that match with the index of the current step
  return values[uniqueVals.indexOf(step)]
}

function rot(steps, pattern){
  if (arguments.length > 2) {
    let multipleValues = []
    // stats at 1, ignore first argument, others added to an array
    for (let i = 1; i < arguments.length; i++) {
      multipleValues.push(arguments[i])
    }
    pattern = multipleValues
  }
  
  steps = steps % pattern.length // Handle cases where steps is greater than array length
  // console.log(steps, pattern, pattern.length);
  pattern =  pattern.slice(-steps).concat(pattern.slice(0, -steps))
  return pattern
}

// euclidean: generate an euclidean pattern
function eucl(beats, steps, val1, val2, cycle){
  val1 = Number(val1)
  val2 = Number(val2)
  let euclPattern = getEuclPattern(beats, steps)
  let step = euclPattern[cycle % euclPattern.length]
  if(step){
    return val1
  }else{
    return val2
  }
}

// glob: generate clusters separated with silence
function gl(amount=10, size=1, silence=8, randRange=1, cycle){
  let a = []
  let inter = size / amount
  for (let i = 0; i < amount; i++) {
    rand = r(-randRange, randRange)
    // adds the silence after the last beat of the glob
    if (i == amount - 1){
      // randomness also aplies to silences, leading to offsets in time
      rand = silence + rand * silence + inter
    }else{
      rand = rand * inter + inter
    }
    a.push(rand)
  }
  let cycleStep = a[cycle % a.length]
  return cycleStep
}

// array arc
// TODO: add randomize?
function arc(nbrItem=10, max=1, min=.1, cycle="$") {
  const a = []
  const step = Math.PI / (Number(nbrItem) - 1)
  for (let i = 0; i < nbrItem; i++) {
    const num = max - Math.sin(i * step) * (max - min)
    a.push(num * 10 / 10)
  }
  let cycleStep = a[cycle % a.length]
  return cycleStep
}

// solo a pattern
function solo(id){
  let allPatterns = sheet.patterns
  let update = false
  allPatterns.forEach(pattern => {
    if (pattern.id != id && !pattern.state.includes('×')){
      pattern.state = "×"+pattern.state
      update = true
    }
  });
  hasSolo = true
  if(update) updateHTMLlog(sheet)
}

// check if a pattern had its solo removed
function unSolo(){
  let allPatterns = sheet.patterns
  let update = false
  hasSolo = false
  allPatterns.forEach(pattern => {
    if (pattern.state.includes('s')){
      hasSolo = true
    }
  })
  if (hasSolo == false) {
    allPatterns.forEach(pattern => {
      if (pattern.state.includes('×')){
        pattern.state = pattern.state.replace('×','')
        update = true
      }
    })
  }
  if(update){
    updateHTMLlog(sheet)
  }
}

// effects, to use in fx only
function expo(a,b){
  // avoid list or array with a separator, see getEffects()
  return "expo»"+a+"»"+b
}
// reverb
function rev(a,b){
  return "rev»"+a+"»"+b // avoid list or array
}
function del(a,b,c){
  return "del»"+a+"»"+b+"»"+c
}

// Interface

// load a file
async function load(file){
  stop()
  await init()
  fetch(file, {cache: "no-store" })
  .then(response => response.text())
  .then((data) => {
    formatAndAppend(data)
    message('New sheet loaded', "log")
  })
}

function loadAddon(addonName){
  // allows array of addonNames  
  if (typeof(addonName) === "object") {
    let i=0
    addonName.forEach(addon => {
      setTimeout(() => {
        let scriptLine = d.createElement('script')
        scriptLine.setAttribute('src','addons/'+addon+'/init.js')
        b.appendChild(scriptLine)
        message(addon+' loaded!', 'log', 2000)        
      }, 100 * i);
      i++
   }); 
  }else{
    let scriptLine = d.createElement('script')
    scriptLine.setAttribute('src','addons/'+addonName+'/init.js')
    b.appendChild(scriptLine)
    message(addonName+' loaded!', 'log', 2000)
  }
}

// drag and drop
editor.addEventListener("dragenter", (e) => {
  editor.classList.add('dragged')
  message("Drop your file here",'log')
  formatAndAppend('')
},false)
editor.addEventListener("drop", (e) => {
  e.preventDefault();
  var file = e.dataTransfer.files[0],
  reader = new FileReader();
  reader.onload = function(event) {
    formatAndAppend(event.target.result)
  };
  reader.readAsText(file);
},false);

// change style (CSS)
function style(cssRules){
  let styleSheet = d.createElement('style')
  styleSheet.classList.add('extra-style')
  styleSheet.innerHTML = cssRules
  b.appendChild(styleSheet)
}
function resetStyle(){
  d.querySelectorAll('.extra-style').forEach(stylesheet => {
    stylesheet.parentNode.removeChild(stylesheet);
  });
}

function message(text,type,time = 1200){ // error, log
  let messageLine = d.createElement('div')
  messageLine.classList.add(`message`)
  messageLine.classList.add(`message-${type}`)
  messageLine.innerHTML = text
  messageBox.appendChild(messageLine)
  setTimeout(() => {
    messageLine.remove()
  }, time);
}

function showLine(beat){  
  let id = beat.id
  let line
  // blink line only if in codemirror’s active scope
  if (editor.querySelector(`[data-key="id"][data-val="${id}"]`)) {
    line = editor.querySelector(`[data-key="id"][data-val="${id}"]`)
    line.style.setProperty('--linecolor',(id * 52))
    line.classList.add('playing')
    setTimeout(() => {
      line.classList.remove('playing')
    }, 30)
  }
}

function theme(theme){
  b.dataset.theme = theme
}

const btns = d.querySelectorAll('footer button')
btns.forEach(el => {
  el.onclick = function(){
    let state = this.dataset.btn
    if (d.body.dataset.state != state) {
      d.body.dataset.state = state
    }else {
      d.body.dataset.state = ""
    }
    if (state == 'start' && started == false) {
      start()
    }else if(state == 'reset'){
      reset()
    }else if(state == 'encode'){
      let encodedURL = 'https://raphaelbastide.com/ete/#'+encode()
      // requires https
      navigator.clipboard.writeText(encodedURL).then(function() {
        message('URL copied to the clipboard','log')
      }, function(err) {
        console.error('Async: Could not copy text: ', err);
      });
    }else if(state == 'start' && started == true){
      stop()
    }
  }
})

// Ask before closing tab
window.onbeforeunload = function (e) {
  if (started) { // only ask if été is started
    stop() // avoid sound event accumulation and super loud thing alert releases
    e = e || window.event; // IE
    if (e) {e.returnValue = 'Any string'}
    return 'Any string'; // safari
  }
};

// Utils

function getID(){return id++}

function solve(val, cycle, forceArray=false, velocity=null){  
  if(typeof val === "undefined") val = String(val)
  // test for arrays, used for playPattern() in rec mode
  if (typeof(val) !== 'string' || forceArray){
    return val
  }else{
    val = val.split(" ").join("")
    val = val.replace('$',cycle)
    if (velocity) {
      val = val.replace('vv',Number(velocity))
    }else{
      val = val.replace('vv',1)
    }
    return transformAndPick(val,cycle)
  }
}

function transformAndPick(string, cycle){  
  let strEvaluated = recursEval(string);
  let array = strEvaluated.split(",");
  return array[cycle % array.length];
}

function recursEval(string){
  // transform repeater ":" and replace it by repeated values
  string = checkRepeats(string)
  // check for FM synth (fm(…) function-like)
  const regex = /fm\((.*?)\)/;
  if (regex.test(string)) {
    // If found, return the entire string without splitting
    let fm = regex.exec(string)[1]
    // avoid (replace) comma to pass the fm synth
    fm = fm.replace(/"/g, "'").replace(/,/g,'œ')
    string = string.replace(regex, JSON.stringify(fm));    
  }  
  // handles math-like patterns
  let isMath = /([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)/g
  // replace itv var, after removing whitespaces
  string = string.split(" ").join("").replace(/itv/g,sheet.config.itv)
  // check if has math formats
  if (string.match(isMath)) {
    let mathList = string.match(isMath)
    // loop through all maths and solve them with eval
    mathList.forEach(math => {
      string = string.replace(math, eval(math))
    })
    solve(Number(string))
  }
  // check for function-like strings: a(b,c,d(e,f))
  let fnLikeList = string.match(/([a-z]*\((?:[A-Za_-z0-9_,\.\#\-]{1,})\))/g)
  // detecting the upper layer
  // "a(b), a(c(x))" returns [a(b), c(x)]
  if(fnLikeList == null){
    // no function-like remain, return the string
    return string;
  }
  //
  // All arguments are surrounded by quotes, then the quotes around numbers are removed.
  // So sh(100_D#2_D,100,sine_noise,E3,E4_E5) => sh("100_D#2_D","100","sine_noise","E3","E4_E5") => sh("100_D#2_D",100,"sine_noise","E3","E4_E5")
  let insideParenthesis = fnLikeList[0].match(/[^()]+(?=\))/g);
  let insideParenthesisModify = '\"' + insideParenthesis + '\"';
  insideParenthesisModify = insideParenthesisModify.replaceAll(/,/g, '\",\"');
  insideParenthesisModify = insideParenthesisModify.replaceAll(/\"(\d+)"/g, '$1');
  insideParenthesisModify = insideParenthesisModify.replaceAll(/\"(\d+.\d+)"/g, '$1');

  let innerToString = fnLikeList[0].replace(insideParenthesis, insideParenthesisModify);
  let temp = string.replace(fnLikeList[0], eval(innerToString));
  // the function calls itself until no function-like remain
  return recursEval(temp);
}

function checkRepeats(string){
  // 3:10 alias to re(3,10) = 10,10,10
  return string.replace(/(\d+):([A-Za_-z0-9_\.\#\-]{1,})/g, (_, count, value) => {
    return re(count,value)
  })
}

function resetCycles(){
  sheet.patterns.forEach(p => {
    p.meta = [0,0,1]
  })
}

function getEuclPattern(beats, steps) {
  // Bjorklund algorithm from https://gist.github.com/withakay/1286731
  beats = Math.round(beats)
  steps = Math.round(steps)
  if (beats > steps || beats == 0 || isNaN(beats) || steps == 0 || isNaN(steps)) {
    console.info('Element needs two values');
    return []
  }
  let pattern = []
  let counts = []
  let remainders = []
  let divisor = steps - beats
  remainders.push(beats)
  let level = 0
  while (true) {
    counts.push(Math.floor(divisor / remainders[level]))
    remainders.push(divisor % remainders[level])
    divisor = remainders[level]
    level += 1
    if (remainders[level] <= 1) {
      break
    }
  }
  counts.push(divisor)
  let r = 0
  const build = function(level) {
    r += 1
    if (level > -1) {
      for (var i = 0; i < counts[level]; i++) {
        build(level - 1)
      }
      if (remainders[level] !== 0) {
        build(level - 2)
      }
    } else if (level === -1) {
      pattern.push(0)
    } else if (level === -2) {
      pattern.push(1)
    }
  }
  build(level)
  return pattern.reverse()
}

function dotStringToObj(obj,is,value) {
  if (typeof is == 'string')
    return dotStringToObj(obj,is.split('.'), value);
  else if (is.length==1 && value!==undefined)
    return obj[is[0]] = value;
  else if (is.length==0)
    return obj;
  else
    return dotStringToObj(obj[is[0]],is.slice(1), value);
}

function sin(d){
  return Math.sin(d)
}
