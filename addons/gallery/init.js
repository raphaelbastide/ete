let galleryLink = d.createElement('link')
galleryLink.rel = 'stylesheet'
galleryLink.type = 'text/css'
galleryLink.href = 'addons/gallery/gallery.css'
d.head.appendChild(galleryLink)
let galleryBox = d.createElement("div")
const imgCount = 41

galleryBox.classList.add("gallery")
b.appendChild(galleryBox)

function gallery(){
  let count = 0
  createImg(count)
  count++
  let int = setInterval(() => {
    if (count < imgCount) {
      createImg(count)
      count++
    }else{
      clearInterval(int)
    }
  }, 60 * 1000 * 0.2);
}

function createImg(count){
  let imgURL = `addons/gallery/images/${count + 1}.png`
  let img = d.createElement("img")
  img.classList.add('g-extend')
  setTimeout(() => {
    img.classList.remove("g-extend")
  }, 1100);
  img.src = imgURL
  galleryBox.appendChild(img)
}