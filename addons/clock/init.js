
var cssLink = document.createElement('link');
cssLink.rel = 'stylesheet';
cssLink.type = 'text/css';
cssLink.href = 'addons/clock/clock.css';
d.head.appendChild(cssLink);
let textBox
let clockStarted = false
let rotation = 50;
let rotint;

function setRotation() {
  rotint = setInterval(() => {
    rotation++
    textBox.style.transform = `rotate(${rotation}deg)`
  }, 1000)
}

function clock(){
  if (clockStarted) {
    textBox.remove()
    clearInterval(rotint)
    clockStarted = false
  }else{
    textBox = d.createElement("div")
    textBox.classList.add("text-box")
    b.appendChild(textBox)
    rotation = 50;

    d.addEventListener("percsynth", (e) => {
      clearInterval(rotint)
      rotation = rotation - 2
      textBox.style.transform = `rotate(${rotation}deg)`
      textBox.style.backgroundColor = `red`
      setTimeout(() => {
        textBox.style.backgroundColor = ``    
      }, 100);
      setRotation()
    })
    clockStarted = true
  }

}