#!/bin/sh
echo "Run this script with bash!"

# First remove everything existing in current dir
rm ./medium/*
then copy the images in ./min
# cp original/*.JPG ./medium

echo "Running rembg…"
# to install it: https://github.com/danielgatis/rembg
rembg p original medium

echo "Running imagemagick autocrop and resize…"
var=0
for f in ./medium/*.png;
  do
    # convert $f -trim +repage $f
  convert $f -trim +repage  -resize 1200 ./medium/$var.png
  convert $f -resize 200 ./min/$var.png
  convert ./min/$var.png -trim +repage -ordered-dither o8x8,6 ./gif/$var.gif
  rm $f
  echo Image $var ✔
  var=$((var+1))
  done
notify-send -t 8000 "End of image processing";
