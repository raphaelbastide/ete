let timerMin = 0
let timerInterval
let timerBox = document.createElement('p')
timerBox.onclick = function(){ resetTimer()}
timerBox.style.cursor = "pointer"
timerBox.classList.add('timer')
timerBox.setAttribute('title','timer in minutes')
timerBox.innerHTML = timerMin
function startTimer(){
  document.body.querySelector('footer').appendChild(timerBox)
  timerInterval = setInterval(() => {
    timerMin++
    timerBox.innerHTML = timerMin
  }, 60000);
}
function resetTimer(){
  clearInterval(timerInterval)
  timerMin = 0
  timerBox.innerHTML = timerMin
  startTimer()
}
startTimer()