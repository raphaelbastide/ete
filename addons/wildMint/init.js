let isWildMint = false
const text = "·····chlorophyll melancholia·····"
let currentLetter = 0
let link = d.createElement('link')
link.rel = 'stylesheet'
link.type = 'text/css'
link.href = 'addons/wildMint/wildmint.css'
d.head.appendChild(link)
let letterBox = d.createElement("div")
letterBox.classList.add("wm-letter-box")
let leaveBox = d.createElement("div")
leaveBox.classList.add("wm-leave-box", "wm-shadow")
let hasLeft = false
let isBridge = false

theme('bright')
function wildMint(){
  if (!isWildMint) {
    b.appendChild(letterBox)
    for (let i = 0; i < 1200; i++) {
      const letterCase = document.createElement("div");
      letterCase.className = "wm-square";
      letterBox.appendChild(letterCase);
      letterCase.addEventListener("mousedown", () => {
        isMouseDown = true;
        hasLeft = false
        injectLetter(letterCase)
      });
      letterCase.addEventListener("mouseup", () => {
        hasLeft = false
        isMouseDown = false;
      });
      letterCase.addEventListener("mouseenter", () => {
        if (isMouseDown) {
          console.log('mouseenter '+isMouseDown);
          console.log('hasleft '+hasLeft);
          if (!hasLeft) {
            injectLetter(letterCase)
          }
        }
      });
    }
    b.appendChild(leaveBox)
    setTimeout(() => {
      leaveBox.classList.add('animate')
    }, 100);
    let isMouseDown = false;
    isWildMint = true
  }else{
    letterBox.remove()
    leaveBox.remove()
  }
}

function injectLetter(el){
  el.classList.add("wm-active")
  el.innerHTML = text[currentLetter]
  if(currentLetter >= text.length - 1){
    currentLetter = 0
  }else{
    currentLetter++
  }
}

function br(){
  if (isBridge) {
    m([0,2,5,8,10,12,19],"state","p")
    m([17,15],"state","m")
    b.dataset.br = "false"
    isBridge = false
  }else{
    m([0,2,5,8,10,12,19],"state","m")
    m([17,15],"state","p")
    b.dataset.br = "true"
    isBridge = true
  }
}
d.addEventListener("playpattern", (e) => {
  if (e.detail == 16) {
    leaveBox.style.display = "none"
    setTimeout(() => {
      leaveBox.style.display = "block"
    }, 100);
  }
});
d.addEventListener("mouseup", () => {
  isMouseDown = false;
});
letterBox.addEventListener("mouseleave", () => {
  console.log('leave');
  hasLeft = true;
});
letterBox.addEventListener("mouseenter", () => {
  console.log('enter');
});