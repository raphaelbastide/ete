<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta name="robots" content="noindex, nofollow">
    <title>Logos</title>
    <style media="screen">
    body{display: flex; flex-wrap: wrap; margin: 0;}
      figure{border:1px solid gray; margin:1%;}
      figure svg{width:400px; height:400px; object-fit: contain;}
      figcaption{text-align:center; color:#333; margin:3px 0;}
    </style>
  </head>
  <body>
<?php
$directory = "optimised-logos";
$imgs = rglob($directory."/{*.svg}", GLOB_BRACE);
$json = [];
$id = 0;
foreach ($imgs as $image) {
  $svgContent = file_get_contents($image);
  $name = pathinfo($image)['filename'];
  echo "<figure>";
  // echo "<img src='$image'/>";
  echo  $svgContent;
  echo  "<figcaption>$name</figcaption>";
  echo "</figure>";
  $json['total'] = $id;
  $json['logos'][$id]['id'] = $id;
  $json['logos'][$id]['destroyed'] = false;
  $json['logos'][$id]['name'] = $name;
  $json['logos'][$id]['svg'] = $svgContent;
  $id++;
}
// $json = "let logoList=".json_encode($json);
$json = json_encode($json);
// Writing json file
$jsonFile = fopen("logos.json", "w") or die("Unable to open file!");
fwrite($jsonFile, $json);
fclose($jsonFile);


// Utils
function rglob($pattern, $flags = 0) {
  $files = glob($pattern, $flags);
  foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
    $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
  }
  return $files;
}

?>
</body>
</html>
