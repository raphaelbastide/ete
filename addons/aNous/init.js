let isANous = false
let link = document.createElement('link')
link.rel = 'stylesheet'
link.type = 'text/css'
link.href = 'addons/aNous/anous.css'
d.head.appendChild(link)
let streetBox = d.createElement("div")
let buildings = 200
streetBox.classList.add('street-box')
let colors = ["SlateGray","gray","RosyBrown","Tomato","LightSlateGray","DarkGray","LightGray","Tan","PowderBlue","yellow","MidnightBlue"]
let bStyles = ["solid","groove","outset","double"]
let grads = ["linear-gradient(-90deg, rgba(0,0,0,.2) -10%, rgba(0,0,0,.2) 50%, rgba(0,0,0,0) 50%, rgba(0,0,0,0) 110%)","linear-gradient(-90deg, rgba(0,0,0,0) -10%,rgba(0,0,0,.1) 110%)","linear-gradient(0deg, rgba(0,0,0,.2) -10%,rgba(0,0,0,.2) 50%,rgba(0,0,0,0) 50%,rgba(0,0,0,0) 110%)","none","none"]

let bWidths = [3,5,10,20,30]
let anId = 0

theme('bright')
function aNous(){
  if (!isANous) {
    b.appendChild(streetBox)
    let sideLeft = d.createElement('aside')
    let sideRight = d.createElement('aside')
    sideLeft.classList.add('an-left') 
    sideRight.classList.add('an-right')
    for (let i = 0; i < buildings; i++) {
      let building = createBuilding()
      sideLeft.appendChild(building)
    }
    for (let i = 0; i < buildings; i++) {
      let building = createBuilding()
      sideRight.appendChild(building)
    }
    streetBox.appendChild(sideLeft)
    streetBox.appendChild(sideRight)
    setTimeout(() => {
      streetBox.classList.add('appear')
    }, 100);
    isANous = true
  }else{
    // remove
    streetBox.remove()
  }
}

function createBuilding(){
  let building = d.createElement('div')
  building.classList.add('building') 
  let r1 = bWidths[Math.floor(Math.random() * bWidths.length)]
  let r2 = colors[Math.floor(Math.random() * colors.length)]
  let r3 = Math.floor(Math.random() * 50) + 5
  let r4 = bStyles[Math.floor(Math.random() * bStyles.length)]
  let r5 = Math.random() > .8
  let r6 = grads[Math.floor(Math.random() * grads.length)]
  let r7 = Math.floor(Math.random() * 10)
  let r8 = Math.floor(Math.random() * 2)
  let r9 = Math.floor(Math.random() * 10)
  building.style.borderWidth = `${r1}px`
  building.style.borderStyle = `${r4}`
  building.style.marginTop = `calc(${+r5} * 4vh)`
  building.style.backgroundColor = `${r2}`
  building.style.backgroundImage = r6
  building.style.setProperty('--id',anId++)
  building.style.setProperty('--h',r3)
  building.style.height = `${r3}vh`
  building.dataset.chem = r7
  building.dataset.in = r8
  building.dataset.shop = r9
  return building
}
