let link = document.createElement('link')
link.rel = 'stylesheet'
link.type = 'text/css'
link.href = 'addons/fonts/stylesheet.css'
d.head.appendChild(link)

function fonts(font){
  style(`:root{--font:26px/var(--lh) '${font}'}`)
}