let link = document.createElement('link')
link.rel = 'stylesheet'
link.type = 'text/css'
link.href = 'addons/keyload/stylesheet.css'
d.head.appendChild(link)

d.addEventListener("keyDown", (e) => {
  let event = e.detail
  if (event.key.startsWith('F') && !isNaN(event.key.charAt(1))) { 
    const number = event.key.charAt(1)
    event.preventDefault()
    load(`sheets/parts/vectrx-${number}.yaml`)
  }
});