# Destroy logot

Addon for été to display SVG logo destruction on keypress

0. It’s best if SVGs have no layer, no clip, no group, no mask. Prefer peparated object than merged path 
1. Convert SVG images with .sh script
2. Open the php file to generate the logos.js file
3. Insert logos.js then destroy-logos.js in été, use it with `destroyLogos()`